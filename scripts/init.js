const branch = 'main';
const baseUrl = `https://gl.githack.com/PReimers/bitrunner-scripts/-/raw/${branch}/scripts/`;
const filesToDownload = [
  'common.js',
  'mainHack.js',
  'spider.js',
  'grow.js',
  'hack.js',
  'weaken.js',
  'playerServers.js',
  'killAll.js',
  'runHacking.js',
  'find.js',
  'solve-contracts.js',
];
const valuesToRemove = ['BB_SERVER_MAP'];

function localeHHMMSS(ms = 0) {
  if (!ms) {
    ms = new Date().getTime();
  }

  return new Date(ms).toLocaleTimeString();
}

/**
* @param {NS} ns
**/
export async function main(ns) {
  ns.tprint(`[${localeHHMMSS()}] Starting init.js`);

  let hostname = ns.getHostname()

  if (hostname !== 'home') {
    throw new Exception('Run the script from home');
  }

  for (let i = 0; i < filesToDownload.length; i++) {
    const filename = filesToDownload[i];
    const path = baseUrl + filename;
    await ns.scriptKill(filename, 'home');
    await ns.rm(filename);
    await ns.sleep(200);
    ns.tprint(`[${localeHHMMSS()}] Trying to download ${path}`);
    await ns.wget(path + `?ts=${new Date().getTime()}`, filename);
  }

  valuesToRemove.map((value) => localStorage.removeItem(value));

  ns.tprint(`[${localeHHMMSS()}] Spawning killAll.js`);
  ns.spawn('killAll.js', 1, 'runHacking.js');
}
