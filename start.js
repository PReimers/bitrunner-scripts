/**
* @param {NS} ns
**/
export async function main(ns) {
    if (ns.getHostname() !== "home") {
        throw new Exception("Please run the script from home");
    }

    await ns.wget(
    `https://gl.githack.com/PReimers/bitrunner-scripts/-/raw/main/scripts/init.js?ts=${new Date().getTime()}`,
    "init.js"
  );
  ns.spawn("init.js", 1);
}
