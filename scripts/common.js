export function settings() {
  return {
    actions: {
      BUY: 'buy',
      UPGRADE: 'upgrade',
    },
    changes: {
      hack: 0.002,
      grow: 0.004,
      weaken: 0.05,
    },
    gbRamCost: 55000,
    homeRamReserved: 20,
    homeRamReservedBase: 20,
    homeRamExtraRamReserved: 12,
    homeRamBigMode: 64,
    keys: {
      serverMap: 'BB_SERVER_MAP',
      hackTarget: 'BB_HACK_TARGET',
      action: 'BB_ACTION',
    },
    minGbRam: 64,
    minSecurityLevelOffset: 1,
    minSecurityWeight: 100,
    mapRefreshInterval: 1 * 60 * 60 * 1000,
    maxGbRam: 1048576,
    maxMoneyMultiplayer: 0.9,
    maxPlayerServers: 25,
    maxWeakenTime: 30 * 60 * 1000,
    totalMoneyAllocation: 0.9,
  }
}

export const hackPrograms = [
  'BruteSSH.exe',
  'FTPCrack.exe',
  'relaySMTP.exe',
  'HTTPWorm.exe',
  'SQLInject.exe'
];

export const hackScripts = [
  'hack.js',
  'grow.js',
  'weaken.js'
];

export function getItem(key) {
  let item = localStorage.getItem(key)

  return item ? JSON.parse(item) : undefined
}

export function setItem(key, value) {
  localStorage.setItem(key, JSON.stringify(value))
}

export function createUUID() {
  var dt = new Date().getTime()
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0
    dt = Math.floor(dt / 16)
    return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
  return uuid
}

export function localeHHMMSS(ms = 0) {
  if (!ms) {
    ms = new Date().getTime();
  }

  return new Date(ms).toLocaleTimeString();
}

export function convertMSToHHMMSS(ms = 0) {
  if (ms <= 0) {
    return '00:00:00'
  }

  if (!ms) {
    ms = new Date().getTime()
  }

  return new Date(ms).toISOString().substr(11, 8)
}

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',')
}

/**
* @param {NS} ns
**/
export function getPlayerDetails(ns) {
  let portHacks = 0

  hackPrograms.forEach((hackProgram) => {
    if (ns.fileExists(hackProgram, 'home')) {
      portHacks += 1
    }
  })

  return {
    hackingLevel: ns.getHackingLevel(),
    portHacks,
  }
}

/**
* @param {NS} ns
**/
export async function main(ns) {
  return {
    settings,
    getItem,
    setItem,
  }
}
